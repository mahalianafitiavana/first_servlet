<%@ page import="java.io.IOException" %>
<%@ page import="info.*,java.util.List" %>
<%
    List<Phone> phone  =  (List) request.getAttribute("phonelist");
    List<Employe> employe  =  (List) request.getAttribute("employelist");
%>

<html>
<head>
  <head>
    <meta charset="utf-8" />
    <meta
      
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />
    <title>New Sale</title>
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="./assets/img/favicon/favicon.ico" />
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="./assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="./assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="./assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />
    <!-- Vendors CSS -->
    <link rel="stylesheet" href="./assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />
    <!-- Script CSS -->
     <script type="text/javascript">
        var salejson =  '<%= (String)  request.getAttribute("salejson") %>';
        console.log(salejson);
    </script>
    <script src="./assets/js/xhr.js"  > </script>
    <script src="./assets/js/saleupdate.js"  > </script>
</head>
<body>
    <div class="col-md-4" style="margin : auto; ">
        <div class="col-md-12 " style=" align: center">
            <div class="card mb-4" style="margin-top: 50px">
                <h5 class="card-header">Create new Sale</h5>
                <form action= "sale" method="post">
                    <div class="card-body">
                            <input type="hidden"  name="id" value="-1" >
                        <div class="mb-3">
                            <label  class="form-label">Phone </label>
                            <select name="idphone"  class="form-select form-select-lg largeSelect">
                                <% for(Phone p : phone) { %>
                                    <option value="<%= p.getId() %>">
                                    <%
                                        Modele m =  new Modele(); m.setId(p.getModele()); m.getById();
                                        Brand b = new Brand(); b.setId(m.getBrand()); b.getById();
                                        Storage s =  new Storage(m.getStorage(),0); s.getById();
                                        out.print(b.getName()+"   "+m.getName()+" "+s.getCapacity()+"Go"); 
                                    %>
                                </option>
                                <% } %>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Employe </label>
                            <select name="idemploye" class="largeSelect form-select form-select-lg">
                                <% for(Employe e : employe) { %>
                                    <option value="<%= e.getId() %>"><%= e.getNom() %></option>
                                <% } %>
                            </div>                        
                            </select>
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Date </label> </br>
                           <input type="date"  name="date"  >
                        </div>
                        <div class="d-grid gap-2 col-lg-6 mx-auto">
                            <button class="btn btn-primary btn-lg" type="submit"id="submit" >Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
<body>