<%@ page import="java.util.List,info.*" %>
<%
    List<Sale> list  =  (List) request.getAttribute("list");
    List<Phone> phone  =  (List) request.getAttribute("listphone");  
    List<Employe> employe  =  (List) request.getAttribute("listemploye");   
%>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />
    <title>Sale</title>
    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="./assets/img/favicon/favicon.ico" />
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="./assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="./assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="./assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="./assets/css/nav.css" />
    <style>
        .search-form {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }
        .search-form input, .search-form select {
            margin-right: 10px;
        }
    </style>
</head>
<body>  
        <div class="row">
            <nav class="col-md-2 d-none d-md-block vertical-menu">
                     <a href="phone">Phone</a>
                    <a href="modele">Modele</a>
                    <a href="sale">Sale</a>
                     <a href="repair">Repair</a>
            </nav>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
        
                <h1 >Sales </h1 >
                    <form class="search-form" action="sale" method="get">
                        <input type="hidden" name="surch" value="0"  />
                        <label for="">Begining:</label>
                        <input type="date" name="min" />
                        <label for="">Ending:</label>
                        <input type="date" name="max"  />
                        <select name="phone" class="form-select">
                            <option value="null"  selected>Phone List</option>
                            <% for(Phone p : phone) { %>
                                <option value="<%= p.getId() %>">
                                <%
                                    Modele m =  new Modele(); m.setId(p.getModele()); m.getById();
                                    Brand b = new Brand(); b.setId(m.getBrand()); b.getById();
                                    Storage s =  new Storage(m.getStorage(),0); s.getById();
                                    out.print(b.getName()+"   "+m.getName()+" "+s.getCapacity()+"Go"); 
                                %>
                            </option>
                            <% } %>                  
                        </select>
                        <select name="employe" class="form-select">
                            <option value="null" selected>Employe List</option>
                            <% for(Employe e : employe) { %>
                                <option value="<%= e.getId() %>"><%= e.getNom() %></option>
                            <% } %>
                        </select>
                        <button type="submit">Search</button>
                    </form>
            <div class="card">
            <h5 class="card-header">Sale List </h5>
            <div class="table-responsive text-nowrap">
                <table class="table">
                <caption class="ms-4">
                    Sale List
                </caption>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Phone </th>
                        <th>Seller</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <% for(Sale s : list) { %>
                    <tr>
                        <td>
                            <strong>
                            <%= s.getDate() %>
                            </strong>
                        </td>
                        <td>
                            <%
                                Phone p = new Phone(); p.setId(s.getIdphone()); p.getById();
                                Modele m =  new Modele(); m.setId(p.getModele()); m.getById();
                                Brand b = new Brand(); b.setId(m.getBrand()); b.getById();
                                Storage st =  new Storage(); st.setId(m.getStorage()); st.getById();
                                out.print(b.getName()+"   "+m.getName()+"  "+st.getCapacity()+"Go");
                            %>
                        </td>
                        <td>
                            <%
                                Employe e = new Employe(); e.setId(s.getIdemploye()); e.getById();
                                out.print(e.getNom());
                            %>
                        </td>
                        <td>
                            <a href="saleForm?action=update&id=<%= s.getId() %>"><i class="bx bx-edit-alt me-2"></i> </a> 
                            <a  href="sale?action=delete&id=<%= s.getId() %>"><i class="bx bx-trash me-2"></i> </a>
                            <a href="saleForm"><i class="bx bxs-add-to-queue me-2"></i> </a>
                        </td>
                    </tr>
                    <% } %>
                </tbody>
                </table>
            </main>
        </div>
<body>  
</html>  

