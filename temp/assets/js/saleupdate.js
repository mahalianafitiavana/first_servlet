window.addEventListener("load", function(){    
    if (salejson === 'null') {
       // tsy manao inin
    }
    else{
        let saleobject = JSON.parse(salejson);   
        modifyTitle(saleobject.id);
        selectedEmploye(saleobject.idemploye);
        selectedPhone(saleobject.idphone);
        selectedDate(saleobject.date);
    }    
});
function modifyTitle(id){
    let p = document.getElementsByTagName('h5')[0];
    p.innerHTML = "Modify Sale";
    let buttonsubmit = document.getElementById("submit");
    buttonsubmit.innerHTML = 'Modify';
    document.getElementsByName('id')[0].value = id;
}
function selectedDate(dateString) {
    // Créer un objet Date à partir de la chaîne de date
    var date = new Date(dateString);

    // Obtenir le jour, le mois et l'année de la date
    var day = date.getDate();
    var month = date.getMonth() + 1; // JavaScript commence le mois à 0, donc nous ajoutons 1
    var year = date.getFullYear();

    // Formater la date selon le format attendu par le champ de date
    var formattedDate = year + '-' + (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day);

    // Définir la valeur du champ de date
    document.getElementsByName('date')[0].value = formattedDate;
}
function selectedEmploye(idemploye){
    let options = document.getElementsByName("idemploye")[0].options;
    for (let i = 0; i < options.length; i++) {
        if (options[i].value == idemploye) {
            options[i].selected = true;
        }
    }
}
function selectedPhone (idphone){
    let options = document.getElementsByName("idphone")[0].options;
    for (let i = 0; i < options.length; i++) {
        if (options[i].value == idphone) {
            options[i].selected = true;
        }
    }
}