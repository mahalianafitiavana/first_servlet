package info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import sgbd.Service;

public class Phone {
    int id;
    double price;
    int status;
    int brand;
    int modele;
    public Phone() {}
    public Phone(int id, double price, int status, int brand, int modele) {
        setBrand(brand);setId(id);setStatus(status);setPrice(price);setModele(modele);
    }
    public int getModele() {
        return modele;
    }
    public void setModele(int modele) {
        this.modele = modele;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public int getBrand() {
        return brand;
    }
    public void setBrand(int brand) {
        this.brand = brand;
    }
    @Override
    public String toString() {
        return "Phone [id=" + getId() + ", price=" + getPrice() + ", status=" + getStatus() + ", brand=" + getBrand() + ", modele=" + getModele()
                + "]";
    }
    public List<Phone> find(Double min, Double max, Integer idbrand, Integer idstatus) throws Exception {
        List<Phone> list = new ArrayList<>();
        Connection c = null;
        PreparedStatement state = null;
        ResultSet result = null;
    
        try {
            c = Service.getConnection();
            StringBuilder sql = new StringBuilder("SELECT * FROM PHONE WHERE 1=1 ");
            if (max != null)  sql.append("AND price <= ? ");
            if (min != null)   sql.append("AND price >= ? ");   
            if (idbrand != null)  sql.append("AND idbrand = ? ");
            if (idstatus != null)  sql.append("AND idstatus = ? ");

            sql.append(" order by idphone asc ");
            state = c.prepareStatement(sql.toString());
            int n = 0;
            
            if (max != null) {
                n++;
                state.setDouble(n, max);
            }
            if (min != null) {
                n++;
                state.setDouble(n, min);
            }
            if (idbrand != null) {
                n++;
                state.setInt(n, idbrand);
            }
            if (idstatus != null) {
                n++;
                state.setInt(n, idstatus);
            }
            
            result = state.executeQuery();
            System.out.println(sql.toString());
            while (result.next()) {
                list.add(new Phone(result.getInt(1), result.getDouble(2), 
                        result.getInt(3), result.getInt(4), result.getInt(5)));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close(); 
            System.out.println("Successful Closing");
        }
        return list;
    }
       
    public void delete () throws Exception{
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "delete from phone  where idphone = ?  ";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getId());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public void getById () throws Exception{
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement("select * from phone where idphone = ?");
            state.setInt(1, this.getId());
            result = state.executeQuery();
            while (result.next() ) {
                this.setPrice(result.getDouble(2));
                this.setStatus(result.getInt(3));
                this.setBrand(result.getInt(4));
                this.setModele(result.getInt(5));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public void update () throws Exception {
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "update phone set price = ?,idstatus = ?, idbrand = ? , idmodele = ? where idphone = ? ";
            state = c.prepareStatement(sql);
            state.setDouble(1, this.getPrice());state.setInt(2, this.getStatus());
            state.setInt(3, this.getBrand());state.setInt(4, this.getModele());
            state.setInt(5, this.getId());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public void save () throws Exception{
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "insert into phone (price, idstatus,idbrand, idmodele) values (?,?,?,?)";
            state = c.prepareStatement(sql);
            state.setDouble(1, this.getPrice());state.setInt(2, this.getStatus());
            state.setInt(3, this.getBrand());state.setInt(4, this.getModele());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public static List <Phone> getall () throws Exception {
        List<Phone> list = new ArrayList<>();
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement("select * from PHONE  order by idphone asc");
            result = state.executeQuery();
            while (result.next() ) {
                list.add(new Phone(result.getInt(1),result.getDouble(2), result.getInt(3),result.getInt(4),result.getInt(5)));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
        return list;
    }
    
}
