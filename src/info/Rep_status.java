package info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import sgbd.Service;

public class Rep_status {
    
    
    int id;
    String desc;
    
    public Rep_status() {
    }
    public Rep_status(int id, String desc) {
        this.id = id;
        this.desc = desc;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public  void findById() throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Service.getConnection(); // Assume Service.getConnection() retourne une connexion valide
            String sql = "SELECT * FROM repair_status WHERE idrep_status=?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, this.getId());
            rs = stmt.executeQuery();

            if (rs.next()) {
                this.setDesc(rs.getString("nom"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        }
    }

    public static List<Rep_status> findAll() throws Exception {
        List<Rep_status> statusList = new ArrayList<>();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Service.getConnection(); // Assume Service.getConnection() retourne une connexion valide
            String sql = "SELECT * FROM repair_status";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                statusList.add(new Rep_status(rs.getInt(1),rs.getString(2)));
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        }
        return statusList;
    }
}
