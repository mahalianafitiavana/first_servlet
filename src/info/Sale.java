package info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import sgbd.Service;
import java.sql.Date; 

public class Sale {
    int id;
    int idphone;
    int idemploye;
    Date date;

    public Sale() {}

    public Sale(int id, int idphone, int idemploye, Date date) {
        setId(id);
        setIdphone(idphone);
        setIdemploye(idemploye);
        setDate(date);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdphone() {
        return idphone;
    }

    public void setIdphone(int idphone) {
        this.idphone = idphone;
    }

    public int getIdemploye() {
        return idemploye;
    }

    public void setIdemploye(int idemploye) {
        this.idemploye = idemploye;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Sale [id=" + getId() + ", idphone=" + getIdphone() + ", idemploye=" + getIdemploye() + ", date=" + getDate() + "]";
    }
    public List<Sale> find(Date min, Date max, Integer idemploye, Integer idphone) throws Exception {
        List<Sale> list = new ArrayList<>();
        Connection c = null;
        PreparedStatement state = null;
        ResultSet result = null;
        try {
            c = Service.getConnection();
            StringBuilder sql = new StringBuilder("SELECT * FROM Sale WHERE 1=1  ");
            if (max != null)  sql.append("AND date <= ? ");
            if (min != null)   sql.append("AND date >= ? ");   
            if (idemploye != null)  sql.append("AND idemploye = ? ");
            if (idphone != null)  sql.append("AND idphone = ? ");

            sql.append(" order by id asc ");
            System.out.println(sql.toString());
    
            state = c.prepareStatement(sql.toString());
            int n = 0;
    
            if (max != null) {
                n++;
                state.setDate(n, max);
            }
            if (min != null) {
                n++;
                state.setDate(n, min);
            }
            if (idemploye != null) {
                n++;
                state.setInt(n, idemploye);
            }
            if (idphone != null) {
                n++;
                state.setInt(n, idphone);
            }
            result = state.executeQuery();
            while (result.next()) {
                Date d = result.getDate(4); 
                list.add(new Sale(result.getInt(1), result.getInt(2), 
                    result.getInt(3), d));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close(); 
            System.out.println("Successful Closing");
        }
        return list;
    }

    public void delete() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "DELETE FROM sale WHERE id = ?";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getId());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
    }

    public void getById() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        ResultSet result = null;
        try {
            c = Service.getConnection();
            state = c.prepareStatement("SELECT * FROM sale WHERE id = ?");
            state.setInt(1, this.getId());
            result = state.executeQuery();
            while (result.next()) {
                this.setIdphone(result.getInt("idphone"));
                this.setIdemploye(result.getInt("idemploye"));
                this.setDate(result.getDate("date"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
    }

    public void update() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "UPDATE sale SET idphone = ?, idemploye = ?, date = ? WHERE id = ?";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getIdphone());
            state.setInt(2, this.getIdemploye());
            state.setDate(3, this.getDate());
            state.setInt(4, this.getId());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
    }

    public void save() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "INSERT INTO sale (idphone, idemploye, date) VALUES (?, ?, ?)";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getIdphone());
            state.setInt(2, this.getIdemploye());
            state.setDate(3, this.getDate());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
    }

    public static List<Sale> getAll() throws Exception {
        List<Sale> list = new ArrayList<>();
        Connection c = null;
        PreparedStatement state = null;
        ResultSet result = null;
        try {
            c = Service.getConnection();
            state = c.prepareStatement("SELECT * FROM sale  order by id asc");
            result = state.executeQuery();
            while (result.next()) {
                list.add(new Sale(result.getInt("id"), result.getInt("idphone"), result.getInt("idemploye"), result.getDate("date")));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
        return list;
    }
}
