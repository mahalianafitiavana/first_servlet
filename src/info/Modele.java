package info;
import sgbd.Service;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
public class Modele {
    int id;
    String  name;
    int brand;
    int storage;
    
    public Modele() {}
    public Modele(int id, String name, int brand, int storage) {
        setId(id);setName(name);setBrand(brand);setStorage(storage);
    }
    public int getStorage() {
        return storage;
    }
    public void setStorage(int storage) {
        this.storage = storage;
    }
    public int getBrand() {
        return brand;
    }
    public void setBrand(int brand) {
        this.brand = brand;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
      
    }
    
    public static List <Modele> getByIdbrand (int id) throws Exception {
        List<Modele> list = new ArrayList<>();
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement("Select * from modele where idbrand = ?  ");
            state.setInt(1, id);
            result = state.executeQuery();
            while (result.next() ) {
                list.add(new Modele(result.getInt("idmodele"),result.getString("name"), result.getInt("idbrand"), result.getInt("idstorage")));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
        return list;
    }
    public void delete () throws Exception{
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "delete from Modele  where idmodele = ? ";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getId());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public void getById () throws Exception{
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement("select * from modele where idmodele = ?");
            state.setInt(1, this.getId());
            result = state.executeQuery();
            while (result.next() ) {
                this.setName(result.getString(2));
                this.setBrand(result.getInt(3));
                this.setStorage(result.getInt(4));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public void update () throws Exception {
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "update modele set name = ?, idbrand = ?, idstorage = ? where idmodele = ? ";
            state = c.prepareStatement(sql);
            state.setString(1, this.getName());
            state.setInt(2, this.getBrand());
            state.setInt(3, this.getStorage());
            state.setInt(4, this.getId());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public void save () throws Exception{
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "insert into Modele (name,idbrand, idstorage) values (?,?,?)";
            state = c.prepareStatement(sql);
            state.setString(1, this.getName());
            state.setInt(2, this.getBrand());
            state.setInt(3, this.getStorage());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
   
    public static List <Modele> getall () throws Exception {
        List<Modele> list = new ArrayList<>();
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement(" select  *  from modele order by idmodele asc");
            result = state.executeQuery();
            while (result.next() ) {
                list.add(new Modele(result.getInt("idmodele"),result.getString("name"), result.getInt("idbrand"), result.getInt("idstorage")));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
        return list;
    }
    public Object find(Integer min, Integer max, Integer brand, String name) throws Exception {
        List<Modele> list = new ArrayList<>();
        Connection c = null; PreparedStatement state = null; ResultSet result = null;
    
        try {
            c = Service.getConnection();
            StringBuilder sql = new StringBuilder("SELECT * FROM Modele m  join Storage s on s.idstorage = m.idstorage WHERE 1=1  ");
            if (max != null)  sql.append("AND capacity <= ? ");
            if (min != null)   sql.append("AND capacity >= ? ");   
            if (brand != null)  sql.append("AND idbrand = ? ");
            if (name != null)  sql.append("AND LOWER(name) like ? ");

            sql.append(" order by m.idmodele asc  ");
            System.out.println(sql.toString());
            state = c.prepareStatement(sql.toString());
            int n = 0;
            
            if (max != null) {
                n++;
                state.setDouble(n, max);
            }
            if (min != null) {
                n++;
                state.setDouble(n, min);
            }
            if (brand != null) {
                n++;
                state.setInt(n, brand);
            }
            if (name != null) {
                n++;
                state.setString(n, "%"+name.trim().toLowerCase()+"%");
            }

            result = state.executeQuery();
            while (result.next()) {
                list.add(new Modele(result.getInt(1), result.getString(2),result.getInt(3), 
                        result.getInt(4)));
            }
        }   catch (Exception e) {
            throw e;
        } finally {
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close(); 
            System.out.println("Successful Closing");
        }
        return list;
    }
    
}
