package info;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import sgbd.Service; 

public class Repair {
    private int idrepair;
    private int idphone;
    private int idemploye;
    private int idstatus;
    private java.sql.Date repairDate;
    private double cost;
    private String description;

    public Repair() {}

    public Repair(int idrepair, int idphone, int idemploye, int idstatus, java.sql.Date repairDate, double cost, String description) {
        this.idrepair = idrepair;
        this.idphone = idphone;
        this.idemploye = idemploye;
        this.idstatus = idstatus;
        this.repairDate = repairDate;
        this.cost = cost;
        this.description = description;
    }

    // Getters and setters

    public int getIdrepair() {
        return idrepair;
    }

    public void setIdrepair(int idrepair) {
        this.idrepair = idrepair;
    }

    public int getIdphone() {
        return idphone;
    }

    public void setIdphone(int idphone) {
        this.idphone = idphone;
    }

    public int getIdemploye() {
        return idemploye;
    }

    public void setIdemploye(int idemploye) {
        this.idemploye = idemploye;
    }

    public int getIdstatus() {
        return idstatus;
    }

    public void setIdstatus(int idstatus) {
        this.idstatus = idstatus;
    }

    public java.sql.Date getRepairDate() {
        return repairDate;
    }

    public void setRepairDate(java.sql.Date repairDate) {
        this.repairDate = repairDate;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Repair> find(Double min, Double max, Integer employe, Integer rep_status) throws Exception{
        List<Repair> list = new ArrayList<>();
        Connection c = null;
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            c = Service.getConnection();
            StringBuilder sql = new StringBuilder("SELECT * FROM Repair WHERE 1=1  ");
            if (max != null)  sql.append("AND cost <= ? ");
            if (min != null)   sql.append("AND cost >= ? ");   
            if (employe != null)  sql.append("AND idemploye = ? ");
            if (rep_status != null)  sql.append("AND idrep_status = ? ");

            sql.append(" order by idrepair asc ");
            System.out.println(sql.toString());
    
            state = c.prepareStatement(sql.toString());
            int n = 0;
    
            if (max != null) {
                n++;
                state.setDouble(n, max);
            }
            if (min != null) {
                n++;
                state.setDouble(n, min);
            }
            if (employe != null) {
                n++;
                state.setInt(n, employe);
            }
            if (rep_status != null) {
                n++;
                state.setInt(n, rep_status);
            }
            rs = state.executeQuery();
            while (rs.next()) { 
                Repair repair = new Repair();
                repair.setIdrepair(rs.getInt("idrepair"));
                repair.setIdphone(rs.getInt("idphone"));
                repair.setIdemploye(rs.getInt("idemploye"));
                repair.setIdstatus(rs.getInt("idrep_status"));
                repair.setRepairDate(rs.getDate("repair_date"));
                repair.setCost(rs.getDouble("cost"));
                repair.setDescription(rs.getString("description"));
                list.add(repair);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) rs.close();
            if (state != null) state.close();
            if (c != null) c.close(); 
            System.out.println("Successful Closing");
        }
        return list;
    }

    public static List<Repair> findAll() throws Exception {
        List<Repair> repairList = new ArrayList<>();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Service.getConnection(); // Assume Service.getConnection() retourne une connexion valide
            String sql = "SELECT * FROM Repair order by idrepair";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Repair repair = new Repair();
                repair.setIdrepair(rs.getInt("idrepair"));
                repair.setIdphone(rs.getInt("idphone"));
                repair.setIdemploye(rs.getInt("idemploye"));
                repair.setIdstatus(rs.getInt("idrep_status"));
                repair.setRepairDate(rs.getDate("repair_date"));
                repair.setCost(rs.getDouble("cost"));
                repair.setDescription(rs.getString("description"));
                repairList.add(repair);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        }
        return repairList;
    }

    public void findById() throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = Service.getConnection(); // Assume Service.getConnection() retourne une connexion valide
            String sql = "SELECT * FROM Repair WHERE idrepair=?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, this.getIdrepair());
            rs = stmt.executeQuery();

            if (rs.next()) {
                this.setIdrepair(rs.getInt("idrepair"));
                this.setIdphone(rs.getInt("idphone"));
                this.setIdemploye(rs.getInt("idemploye"));
                this.setIdstatus(rs.getInt("idrep_status"));
                this.setRepairDate(rs.getDate("repair_date"));
                this.setCost(rs.getDouble("cost"));
                this.setDescription(rs.getString("description"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        }
    }

    public void save() throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = Service.getConnection(); // Assume Service.getConnection() retourne une connexion valide
            String sql = "INSERT INTO Repair (idphone, idemploye, idrep_status, repair_date, cost, description) VALUES (?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, this.idphone);
            stmt.setInt(2, this.idemploye);
            stmt.setInt(3, this.idstatus);
            stmt.setDate(4, this.repairDate);
            stmt.setDouble(5, this.cost);
            stmt.setString(6, this.description);
            stmt.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        }
    }

    public void update() throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = Service.getConnection(); // Assume Service.getConnection() retourne une connexion valide
            String sql = "UPDATE Repair SET idphone=?, idemploye=?, idrep_status=?, repair_date=?, cost=?, description=? WHERE idrepair=?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, this.idphone);
            stmt.setInt(2, this.idemploye);
            stmt.setInt(3, this.idstatus);
            stmt.setDate(4, this.repairDate);
            stmt.setDouble(5, this.cost);
            stmt.setString(6, this.description);
            stmt.setInt(7, this.idrepair);
            stmt.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        }
    }

    public void delete() throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = Service.getConnection(); // Assume Service.getConnection() retourne une connexion valide
            String sql = "DELETE FROM Repair WHERE idrepair=?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, this.idrepair);
            stmt.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        }
    }
}
