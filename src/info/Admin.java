package info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import sgbd.Service;

public class Admin {
    int id;
    String login;
    String pwd;

    public Admin() {}

    public Admin(int id, String login, String pwd) {
        setId(id);
        setLogin(login);
        setPwd(pwd);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "Admin [id=" + getId() + ", login=" + getLogin() + ", pwd=" + getPwd() + "]";
    }

    public  String connect() throws Exception {
        Connection c = null; PreparedStatement state = null; ResultSet result = null;

        try {
            c = Service.getConnection();
            state = c.prepareStatement("SELECT * FROM admin WHERE login = ?");
            state.setString(1, this.getLogin());
            result = state.executeQuery();

            if (result.next()) {
                String storedPwd = result.getString("pwd");
                
                if (storedPwd.equals(this.getPwd())) {

                } else {
                    throw new Exception("Wrong password");
                }
            } else {
                throw new Exception("You are not an admin");
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close();
        }
        return "Successful Closing" ;
    }
    
}
