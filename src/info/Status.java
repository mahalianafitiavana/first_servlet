package info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import sgbd.Service;

public class Status {
    int id;
    String desc;

   
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public Status(int id, String nom) {
        setId(id);setDesc(nom);
    }
    public Status() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
   
    @Override
    public String toString() {
        return "status [id=" + getId() + ", nom=" + getDesc() + "]";
    }
    public void delete () throws Exception{
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "delete from status  where idstatus = ? ";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getId());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public void getById () throws Exception{
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement("select * from status where idstatus = ?");
            state.setInt(1, this.getId());
            result = state.executeQuery();
            while (result.next() ) {
                this.setDesc(result.getString(2));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close(); System.out.println("Successful Closing");
        }
    }

    public void update () throws Exception {
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "update status set description = ? where idstatus = ? ";
            state = c.prepareStatement(sql);
            state.setString(1, this.getDesc());
            state.setInt(2, this.getId());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public void save () throws Exception{
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "insert into Status (description) values (?)";
            state = c.prepareStatement(sql);
            state.setString(1, this.getDesc());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    public static List <Status> getall () throws Exception {
        List<Status> list = new ArrayList<>();
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement("select distinct * from status");
            result = state.executeQuery();
            while (result.next() ) {
                list.add(new Status(result.getInt(1),result.getString(2)));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
        return list;
    }
    
    
}
