package info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import sgbd.Service;

public class Brand {
    int id;
    String name;
    @Override
    public String toString() {
        return "Brand [id=" + id + ", name=" + name + "]";
    }
    public Brand() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Brand(int id, String name) {
        setId(id);setName(name);
    }
    public void delete () throws Exception{
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "delete from Brand  where idbrand = ? ";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getId());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    
    public void getById () throws Exception{
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement("select * from brand where idbrand = ?");
            state.setInt(1, this.getId());
            result = state.executeQuery();
            while (result.next() ) {
                this.setName(result.getString(2));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close(); System.out.println("Successful Closing");
        }
    }

    public void update () throws Exception {
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "update Brand set name = ? where idbrand = ? ";
            state = c.prepareStatement(sql);
            state.setString(1, this.getName());
            state.setInt(2, this.getId());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }

    public void save () throws Exception{
        Connection c =  null; PreparedStatement state = null; 
        try {
            c = Service.getConnection(); 
            String sql = "insert into Brand (name) values (?)";
            state = c.prepareStatement(sql);
            state.setString(1, this.getName());
            state.executeUpdate();            
        } catch (Exception e) {
            throw e;
        }
        finally{
            if(state != null) state.close();
            if(c != null) c.close(); System.out.println("Successful Closing");
        }
    }
    
    public static List<Brand> getall () throws Exception{
        List<Brand> list = new ArrayList<>();
        Connection c =  null; PreparedStatement state = null; ResultSet result = null;
        try {
            c = Service.getConnection(); 
            state = c.prepareStatement("select * from brand");
            result = state.executeQuery();
            while (result.next() ) {
                list.add(new Brand(result.getInt(1),result.getString(2)));
            }
        } catch (Exception e) {
            throw e;
        }
        finally{
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close(); System.out.println("Successful Closing");
        }
        return list;
    }
}
