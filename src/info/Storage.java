package info;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import sgbd.Service;

public class Storage {
    int id;
    int capacity;

    public int getId() {
        return id;
    }

    public void setId(int idStorage) {
        this.id = idStorage;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Storage(int idStorage, int capacity) {
        setId(idStorage);
        setCapacity(capacity);
    }

    public Storage() {
    }

    @Override
    public String toString() {
        return "Storage [id=" + getId() + ", capacity=" + getCapacity() + "]";
    }

    public void delete() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "delete from storage where idstorage = ? ";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getId());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null)
                state.close();
            if (c != null)
                c.close();
            System.out.println("Successful Closing");
        }
    }

    public void getById() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        ResultSet result = null;
        try {
            c = Service.getConnection();
            state = c.prepareStatement("select * from storage where idstorage = ?");
            state.setInt(1, this.getId());
            result = state.executeQuery();
            while (result.next()) {
                this.setCapacity(result.getInt("capacity"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null)
                result.close();
            if (state != null)
                state.close();
            if (c != null)
                c.close();
            System.out.println("Successful Closing");
        }
    }

    public void update() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "update storage set capacity = ? where idstorage = ? ";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getCapacity());
            state.setInt(2, this.getId());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null)
                state.close();
            if (c != null)
                c.close();
            System.out.println("Successful Closing");
        }
    }

    public void save() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "insert into storage (capacity) values (?)";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getCapacity());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null)
                state.close();
            if (c != null)
                c.close();
            System.out.println("Successful Closing");
        }
    }

    public static List<Storage> getAll() throws Exception {
        List<Storage> list = new ArrayList<>();
        Connection c = null;
        PreparedStatement state = null;
        ResultSet result = null;
        try {
            c = Service.getConnection();
            state = c.prepareStatement("select distinct * from storage");
            result = state.executeQuery();
            while (result.next()) {
                list.add(new Storage(result.getInt("idstorage"), result.getInt("capacity")));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null)
                result.close();
            if (state != null)
                state.close();
            if (c != null)
                c.close();
            System.out.println("Successful Closing");
        }
        return list;
    }
}

