package info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import sgbd.Service;

public class Employe {
    int id;
    String nom;
    double salaire;

    public Employe() {}

    public Employe(int id, String nom, double salaire) {
        setId(id);
        setNom(nom);
        setSalaire(salaire);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    @Override
    public String toString() {
        return "Employe [id=" + getId() + ", nom=" + getNom() + ", salaire=" + getSalaire() + "]";
    }

    public void delete() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "DELETE FROM employe WHERE id = ?";
            state = c.prepareStatement(sql);
            state.setInt(1, this.getId());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
    }

    public void getById() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        ResultSet result = null;
        try {
            c = Service.getConnection();
            state = c.prepareStatement("SELECT * FROM employe WHERE id = ?  order by id asc");
            state.setInt(1, this.getId());
            result = state.executeQuery();
            while (result.next()) {
                this.setNom(result.getString("nom"));
                this.setSalaire(result.getDouble("salaire"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
    }

    public void update() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "UPDATE employe SET nom = ?, salaire = ? WHERE id = ?";
            state = c.prepareStatement(sql);
            state.setString(1, this.getNom());
            state.setDouble(2, this.getSalaire());
            state.setInt(3, this.getId());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
    }

    public void save() throws Exception {
        Connection c = null;
        PreparedStatement state = null;
        try {
            c = Service.getConnection();
            String sql = "INSERT INTO employe (nom, salaire) VALUES (?, ?)";
            state = c.prepareStatement(sql);
            state.setString(1, this.getNom());
            state.setDouble(2, this.getSalaire());
            state.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
    }

    public static List<Employe> getAll() throws Exception {
        List<Employe> list = new ArrayList<>();
        Connection c = null;
        PreparedStatement state = null;
        ResultSet result = null;
        try {
            c = Service.getConnection();
            state = c.prepareStatement("SELECT * FROM employe order by id asc ");
            result = state.executeQuery();
            while (result.next()) {
                list.add(new Employe(result.getInt("id"), result.getString("nom"), result.getDouble("salaire")));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (result != null) result.close();
            if (state != null) state.close();
            if (c != null) c.close();
            System.out.println("Successful Closing");
        }
        return list;
    }
}
