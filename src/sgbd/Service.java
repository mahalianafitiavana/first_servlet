package sgbd;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Service {
    static Connection connection;
    static String url = "jdbc:postgresql://localhost:5432/phone";
    static String username = "postgres";
    static String password = "postgres";
    static String pilote = "org.postgresql.Driver";
    public static Connection getConnection() throws Exception {
        try {
            Class.forName(getPilote());
            setConnection( DriverManager.getConnection(getUrl(), getUsername(), getPassword()));            
            System.out.println("Successful Opening");
        } 
        catch (ClassNotFoundException e) {
            System.out.println("Erreur : Pilote JDBC introuvable.");
            throw e;
        } 
        catch (SQLException e) {
            System.out.println("Erreur : Impossible de se connecter à la base de données.");
            throw e;
        }
        return connection;
    }
    public static void setConnection(Connection connection) {
        Service.connection = connection;
    }
    public static String getUrl() {
        return url;
    }
    public static void setUrl(String url) {
        Service.url = url;
    }
    public static String getUsername() {
        return username;
    }
    public static void setUsername(String username) {
        Service.username = username;
    }
    public static String getPassword() {
        return password;
    }
    public static void setPassword(String password) {
        Service.password = password;
    }
    public static String getPilote() {
        return pilote;
    }
    public static void setPilote(String pilote) {
        Service.pilote = pilote;
    }
}

