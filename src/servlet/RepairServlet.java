package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import info.Employe;
import info.Rep_status;
import info.Repair;


public class RepairServlet extends HttpServlet {
    protected void check_admin(HttpServletRequest req,HttpServletResponse resp ) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("admin") == null) {
            resp.sendRedirect("index.jsp");
        }
    }
    protected void read(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        req.setAttribute("statuslist", Rep_status.findAll());
        req.setAttribute("listemploye", Employe.getAll());
        req.setAttribute("repairlist", Repair.findAll()); // Liste de téléphones
        if (req.getParameter("surch") != null) {
            Double min = null;
            Double max = null;
            Integer rep_status = null;
            Integer employe = null;
            
            if (req.getParameter("min") != null && !req.getParameter("min").isEmpty())
                min = Double.parseDouble(req.getParameter("min"));
            if (req.getParameter("max") != null && !req.getParameter("max").isEmpty())
                max = Double.parseDouble(req.getParameter("max"));
            if (!"null".equals(req.getParameter("rep_status")))
                rep_status = Integer.parseInt(req.getParameter("rep_status"));
            if (!"null".equals(req.getParameter("employe")))
                employe = Integer.parseInt(req.getParameter("employe"));

            Repair p = new Repair();
            req.setAttribute("repairlist", p.find(min, max, employe, rep_status)); // Liste de téléphones filtrée
        }
    }
    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, Exception {
        if (req.getParameter("action") != null && req.getParameter("action").equals("delete")) {
            Repair b =  new Repair();b.setIdrepair(Integer.parseInt(req.getParameter("id")));
            b.delete();
            resp.sendRedirect("repair");
        }
    }
    protected void create(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (req.getParameter("id").equals("-1")) {
            int idemploye = Integer.parseInt(req.getParameter("idemploye"));
            int idphone = Integer.parseInt(req.getParameter("idphone"));
            int idstatus = Integer.parseInt(req.getParameter("rep_status"));
            String desc = req.getParameter("desc");
            double cost = Double.parseDouble(req.getParameter("cost"));
            Date date = Date.valueOf(req.getParameter("date"));
            Repair m = new Repair(0, idphone, idemploye, idstatus, date, cost, desc);
            m.save();
        }
    }
    protected void update(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (!req.getParameter("id").equals("-1")) {
            int id = Integer.parseInt(req.getParameter("id"));
            int idemploye = Integer.parseInt(req.getParameter("idemploye"));
            int idphone = Integer.parseInt(req.getParameter("idphone"));
            int idstatus = Integer.parseInt(req.getParameter("rep_status"));
            String desc = req.getParameter("desc");
            double cost = Double.parseDouble(req.getParameter("cost"));
            Date date = Date.valueOf(req.getParameter("date"));
            Repair m = new Repair(id, idphone, idemploye, idstatus, date, cost, desc);
            m.update();
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            check_admin(req, resp);
            delete(req, resp);
            read(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/repairlist.jsp");
            dispatcher.forward(req, resp);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            check_admin(req, resp);
            create(req, resp);
            update(req, resp);
            read(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/repairlist.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
