package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import info.Brand;
import info.Modele;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ModeleServlet extends HttpServlet {
    protected void check_admin(HttpServletRequest req,HttpServletResponse resp ) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("admin") == null) {
            resp.sendRedirect("index.jsp");
        }
    }
    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (req.getParameter("action") != null && req.getParameter("action").equals("delete")) {
            int id = Integer.parseInt(req.getParameter("id"));
            Modele m =  new Modele();m.setId(id);
            m.delete();
            resp.sendRedirect("modele");
        }
    }
    protected void create(HttpServletRequest req, HttpServletResponse resp) throws  Exception {
        if (req.getParameter("id").equals("-1")) {
            int idbrand = Integer.parseInt(req.getParameter("idbrand"));
            int idstorage = Integer.parseInt(req.getParameter("idstorage"));
            Modele m =  new Modele(0, req.getParameter("name"), idbrand,idstorage);
            m.save();
        }
    }
    protected void read(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        req.setAttribute("list", Modele.getall());
        req.setAttribute("listbrand", Brand.getall());
        if (req.getParameter("surch") != null) {
            Integer min = null;  Integer max = null;  Integer brand = null; String name = null ;
            
            if (req.getParameter("min") != null && !req.getParameter("min").isEmpty())
                min = Integer.parseInt(req.getParameter("min"));
            if (req.getParameter("max") != null && !req.getParameter("max").isEmpty())
                max = Integer.parseInt(req.getParameter("max"));
            if (!"null".equals(req.getParameter("brand")))
                brand = Integer.parseInt(req.getParameter("brand"));
            if ( req.getParameter("name") != null && !req.getParameter("name").isEmpty())
                name = req.getParameter("name");

            Modele m = new Modele();
            req.setAttribute("list", m.find(min, max, brand, name)); // Liste de téléphones filtrée
        }
    }
    protected void update(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (!req.getParameter("id").equals("-1")) {
            int id = Integer.parseInt(req.getParameter("id"));
            int idbrand = Integer.parseInt(req.getParameter("idbrand"));            
            int idstorage = Integer.parseInt(req.getParameter("idstorage"));
            Modele m =  new Modele(id, req.getParameter("name"), idbrand,idstorage);    
            m.update();
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out= null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            delete(req, resp);
            read(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/modelelist.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace(out);
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out= null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            create(req, resp);
            update(req, resp);
            read(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/modelelist.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            out.print(e.getMessage());
        }
    }
}
