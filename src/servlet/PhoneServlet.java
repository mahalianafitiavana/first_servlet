package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import info.Brand;
import info.Phone;
import info.Status;
import info.Storage;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

public class PhoneServlet extends HttpServlet {
    protected void check_admin(HttpServletRequest req,HttpServletResponse resp ) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("admin") == null) {
            resp.sendRedirect("index.jsp");
        }
    }
    private void update(HttpServletRequest req, HttpServletResponse resp) throws Exception {
       if (!req.getParameter("id").equals("-1")) {
            int  id = Integer.parseInt(req.getParameter("id"));
            double price = Double.parseDouble(req.getParameter("price"));
            int status = Integer.parseInt(req.getParameter("idstatus"));
            int brand = Integer.parseInt(req.getParameter("idbrand"));
            int modele = Integer.parseInt(req.getParameter("idmodele"));
            Phone phone = new Phone(id, price, status, brand, modele);
            phone.update(); 
       }
    }
    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, Exception {
        if (req.getParameter("action") != null && req.getParameter("action").equals("delete")) {
            Phone b =  new Phone();b.setId(Integer.parseInt(req.getParameter("id")));
            b.delete();
            resp.sendRedirect("phone");
        }
    }
    protected void create (HttpServletRequest req, HttpServletResponse resp) throws Exception{
        if (req.getParameter("id").equals("-1"))  {
            double price = Double.parseDouble(req.getParameter("price"));
            int status = Integer.parseInt(req.getParameter("idstatus"));
            int brand = Integer.parseInt(req.getParameter("idbrand"));
            int modele = Integer.parseInt(req.getParameter("idmodele"));
            Phone phone = new Phone(0, price, status, brand, modele);
            phone.save(); 
        }
    }
    protected void read(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        req.setAttribute("phonelist", Phone.getall()); // Liste de téléphones
        req.setAttribute("storagelist", new Gson().toJson(Storage.getAll()));
    
        if (req.getParameter("surch") != null) {
            Double min = null;
            Double max = null;
            Integer status = null;
            Integer brand = null;
            
            if (req.getParameter("min") != null && !req.getParameter("min").isEmpty())
                min = Double.parseDouble(req.getParameter("min"));
            if (req.getParameter("max") != null && !req.getParameter("max").isEmpty())
                max = Double.parseDouble(req.getParameter("max"));
            if (!"null".equals(req.getParameter("status")))
                status = Integer.parseInt(req.getParameter("status"));
            if (!"null".equals(req.getParameter("brand")))
                brand = Integer.parseInt(req.getParameter("brand"));
            
            Phone p = new Phone();
            req.setAttribute("phonelist", p.find(min, max, brand, status)); // Liste de téléphones filtrée
        }
    
        req.setAttribute("statuslist", Status.getall()); // Liste des statuts
        req.setAttribute("brandlist", Brand.getall()); // Liste des marques
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out= null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            delete(req, resp);
            read(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/phonelist.jsp");
            dispatcher.forward(req, resp);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if (out != null) 
                out.close();
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out= null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            create(req, resp);
            update(req, resp);
            read(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/phonelist.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
           out.print(e.getMessage());
        }
        finally{
            if (out != null) 
                out.close();
        }
    }
}
