package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import com.google.gson.Gson;
import info.Employe;
import info.Phone;
import info.Sale;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SaleFormServlet extends HttpServlet {
    protected void check_admin(HttpServletRequest req,HttpServletResponse resp ) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("admin") == null) {
            resp.sendRedirect("index.jsp");
        }
    }
    protected void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, Exception {
        String action = req.getParameter("action");
        req.setAttribute("employelist", Employe.getAll()); // liste de employe
        req.setAttribute("phonelist", Phone.getall()); // liste de phone
        if (action != null  && action.equals("update")) {
            Sale m =  new Sale(); m.setId(Integer.parseInt(req.getParameter("id")));
            m.getById();
            String datastring = new Gson().toJson(m);
            req.setAttribute("salejson", datastring);
        }
        
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
          PrintWriter out = null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            update(req, resp);
            RequestDispatcher dispatcher =  req.getRequestDispatcher("/saleForm.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            out.print(e.getMessage());
        }
    }
    
}
