package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import info.Employe;
import info.Phone;
import info.Rep_status;
import info.Repair;

public class RepairFormServlet extends HttpServlet {
    protected void check_admin(HttpServletRequest req,HttpServletResponse resp ) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("admin") == null) {
            resp.sendRedirect("index.jsp");
        }
    }

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, Exception {
        String action = req.getParameter("action");
        req.setAttribute("statuslist", Rep_status.findAll());
        req.setAttribute("listemploye", Employe.getAll());
        req.setAttribute("listphone", Phone.getall());
        if (action != null  && action.equals("update")) {
            Repair r =  new Repair(); r.setIdrepair(Integer.parseInt(req.getParameter("id")));
            r.findById();
            String datastring = new Gson().toJson(r);
            req.setAttribute("repairjson", datastring);
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
          PrintWriter out = null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            processRequest(req, resp);
            RequestDispatcher dispatcher =  req.getRequestDispatcher("/repairForm.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            out.print(e.getMessage());
        }
    }
}
