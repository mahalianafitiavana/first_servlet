package servlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import com.google.gson.Gson;
import info.Brand;
import info.Modele;
import info.Phone;
import info.Status;
import info.Storage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PhoneFormServlet extends HttpServlet{
    protected void check_admin(HttpServletRequest req,HttpServletResponse resp ) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("admin") == null) {
            resp.sendRedirect("index.jsp");
        }
    }
    protected void sender(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, Exception {
        String action = req.getParameter("action");
        req.setAttribute("brandlist", Brand.getall()); // liste de brand
        req.setAttribute("statuslist", Status.getall()); // liste de status
        String storagestring = new Gson().toJson(Storage.getAll());// liste de storage
        req.setAttribute("storagelist",storagestring); 
        if (action != null  && action.equals("update")) {
            Phone m =  new Phone(); m.setId(Integer.parseInt(req.getParameter("id")));
            m.getById();
            String datastring = new Gson().toJson(m);
            req.setAttribute("phonejson", datastring);
        }
        
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
          PrintWriter out = null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            sender(req, resp);
            RequestDispatcher dispatcher =  req.getRequestDispatcher("/phoneForm.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            out.print(e.getMessage());
        }
        finally{
            out.close();
        }
    }
    
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        try {
            check_admin(req, resp);
            // Vérifier si le paramètre 'idbrand' est présent dans la requête
            String idbrandParam = req.getParameter("idbrand");
            if (idbrandParam != null && !idbrandParam.isEmpty()) {
                // Convertir 'idbrand' en un entier
                int idbrand = Integer.parseInt(idbrandParam);

                // Utiliser 'idbrand' pour obtenir la liste des modèles
                List<Modele> modelList = Modele.getByIdbrand(idbrand);

                // Convertir la liste des modèles en JSON avec Gson
                String modelString = new Gson().toJson(modelList);

                // Envoyer la réponse JSON au client
                out.print(modelString);
            } else {
                // Envoyer une réponse d'erreur si 'idbrand' n'est pas présent
                out.print("{\"error\": \"idbrand parameter not found.\"}");
            }
        } catch (Exception e) {
            out.print("{\"error\": \"An error occurred: " + e.getMessage() + "\"}");
        } finally {
            out.close();
        }
    }
}
