package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import info.Employe;
import info.Phone;
import info.Sale;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SaleServlet extends HttpServlet{
    protected void check_admin(HttpServletRequest req,HttpServletResponse resp ) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("admin") == null) {
            resp.sendRedirect("index.jsp");
        }
    }
    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (req.getParameter("action") != null && req.getParameter("action").equals("delete")) {
            int id = Integer.parseInt(req.getParameter("id"));
            Sale m =  new Sale();m.setId(id);
            m.delete();
            resp.sendRedirect("sale");
        }
    }
    protected void create(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (req.getParameter("id").equals("-1")) {
            int idemploye = Integer.parseInt(req.getParameter("idemploye"));
            int idphone = Integer.parseInt(req.getParameter("idphone"));
            Date date = Date.valueOf(req.getParameter("date"));
            Sale m = new Sale(0, idphone, idemploye, date);
            m.save();
        }
    }
    protected void read(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        req.setAttribute("list", Sale.getAll());
        req.setAttribute("listphone", Phone.getall());
        req.setAttribute("listemploye", Employe.getAll());
        
        if (req.getParameter("surch") != null) {
            Date min = null;Date max = null;
            Integer phone = null; Integer employe = null;
            
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    
            if (req.getParameter("min") != null && !req.getParameter("min").isEmpty())
                min = new Date(format.parse(req.getParameter("min")).getTime());
            if (req.getParameter("max") != null && !req.getParameter("max").isEmpty())
                max = new Date(format.parse(req.getParameter("max")).getTime());
            if (!"null".equals(req.getParameter("phone"))) 
                phone = Integer.parseInt(req.getParameter("phone"));
            if (!"null".equals(req.getParameter("employe"))) 
                employe = Integer.parseInt(req.getParameter("employe"));

            Sale s = new Sale();
            req.setAttribute("list", s.find(min, max, employe, phone)); // Liste de vente filtrée
        }
    }
    
    protected void update(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        if (!req.getParameter("id").equals("-1")) {
            int id = Integer.parseInt(req.getParameter("id"));
            int idemploye = Integer.parseInt(req.getParameter("idemploye"));
            int idphone = Integer.parseInt(req.getParameter("idphone"));
            Date date = Date.valueOf(req.getParameter("date"));
            Sale m = new Sale(id, idphone, idemploye, date);    
            m.update();
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out= null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            delete(req, resp);
            read(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/salelist.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace(out);
        }finally {
            if(out != null)
                out.close();
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out= null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            create(req, resp);
            update(req, resp);
            read(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/salelist.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            out.print(e.getMessage());
        }finally{
            if(out != null)
                out.close();
        }
    }
}
