package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import com.google.gson.Gson;
import info.Brand;
import info.Modele;
import info.Storage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ModeleFormServlet extends HttpServlet {
    protected void sender(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, Exception {
        String action = req.getParameter("action");
        req.setAttribute("brandlist", Brand.getall()); // liste de modele
        req.setAttribute("storagelist", Storage.getAll()); // liste de stoarage
        if (action != null  && action.equals("update")) {
            Modele m =  new Modele(); m.setId(Integer.parseInt(req.getParameter("id")));
            m.getById();
            String datastring = new Gson().toJson(m);
            req.setAttribute("modelejson", datastring);
        }
    }
    protected void check_admin(HttpServletRequest req,HttpServletResponse resp ) throws IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("admin") == null) {
            resp.sendRedirect("index.jsp");
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = null;
        try {
            check_admin(req, resp);
            out = resp.getWriter();
            sender(req, resp);
            RequestDispatcher dispatcher =  req.getRequestDispatcher("/modeleForm.jsp");
            dispatcher.forward(req, resp);
        } catch (Exception e) {
            out.print(e.getMessage());
        }
    }
}
