package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import info.Admin;

public class ConnexionForm extends HttpServlet {
    

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = null;
        try{
            out = resp.getWriter();
            Admin a =  new Admin(0,req.getParameter("login"),req.getParameter("pwd"));
            out.println(a.toString());
            out.print( a.connect());
            HttpSession session = req.getSession();
            session.setAttribute("admin", a);
            resp.sendRedirect("phone");
        }
        catch(Exception e){
            resp.sendRedirect("index.jsp?error="+e.getMessage());
        }
    }
    
}
