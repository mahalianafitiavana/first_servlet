create  table admin (
   id Serial,
   login varchar(50),
   pwd varchar(50)
);
CREATE TABLE Brand(
   idbrand SERIAL,
   name VARCHAR(50) ,
   PRIMARY KEY(idbrand)
);

CREATE TABLE Status(
   idstatus SERIAL,
   description VARCHAR(50) ,
   PRIMARY KEY(idstatus)
);

CREATE table Storage (
   idstorage SERIAL,
   capacity INTEGER NOT NULL,
   PRIMARY KEY(idstorage)
);

CREATE TABLE Modele(
   idmodele SERIAL,
   name VARCHAR(50) ,
   idbrand INTEGER NOT NULL,
   PRIMARY KEY(idmodele),
   FOREIGN KEY(idbrand) REFERENCES Brand(idbrand)
);
--add idstorage to Phone
alter  table modele  add idstorage INTEGER not null;
alter table modele  add foreign key (idstorage) references Storage(idstorage);

CREATE TABLE Users(
   iduser SERIAL,
   login VARCHAR(50) ,
   pwd VARCHAR(50) ,
   PRIMARY KEY(iduser)
);



CREATE TABLE Phone(
   idphone SERIAL,
   price NUMERIC(18,2)  ,
   idstatus INTEGER NOT NULL,
   idbrand INTEGER NOT NULL,
   PRIMARY KEY(idphone),
   FOREIGN KEY(idstatus) REFERENCES Status(idstatus),
   FOREIGN KEY(idbrand) REFERENCES Brand(idbrand)
);
alter  table PHONE add IDMODELE INTEGER not null;
alter table PHONE add foreign key (IDMODELE) references MODELE(IDMODELE);



CREATE TABLE employe (
    id SERIAL PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    salaire DECIMAL(18,2) NOT NULL
);


CREATE TABLE Sale (
   id SERIAL,
   idphone INTEGER NOT NULL,
   idemploye INTEGER NOT NULL,
   date DATE,
   PRIMARY KEY(id),
   FOREIGN KEY(idphone) REFERENCES Phone(idphone),
   FOREIGN KEY(idemploye) REFERENCES employe(id)
);

create table repair_status(
	idrep_status serial,
	nom varchar(50),
	primary key (idrep_status)
);
CREATE TABLE Repair (
    idrepair SERIAL,
    idphone INTEGER NOT NULL,
    idemploye INTEGER NOT NULL,
    idrep_status INTEGER NOT NULL,
    repair_date DATE NOT NULL,
    cost NUMERIC(18,2) NOT NULL,
    description TEXT,
    PRIMARY KEY(idrepair),
    FOREIGN KEY(idphone) REFERENCES Phone(idphone),
    FOREIGN KEY(idemploye) REFERENCES Employe(id),
    FOREIGN KEY(idrep_status) REFERENCES repair_status(idrep_status)
);


