@echo off
setlocal
@REM  temp folder
set "temp=C:\Users\Mahaliana\Documents\Git\first_servlet\temp"
@REM lib folder
set "lib=%cd%\lib"
@REM web folder   
set "web=%cd%\web"  
@REM web.xml file                          
set "xml=%cd%\web.xml"   
@REM SRC
set "src=%cd%\src"
@REM webapps folder
set "webapps=C:\xampp\tomcat\webapps"

@REM Supprime le dossier temporaire 
rmdir /s /q "%temp%"
echo Dossier temporaire supprimé. 

@REM Recrée le dossier temporaire
mkdir "%temp%"

@REM Copie web vers temp
xcopy /s /e /y "%web%" "%temp%"

@REM Copie web vers lib
xcopy /s /e /y "%lib%" "%temp%\WEB-INF\lib\"

@REM Copie web vers xml
copy /Y %xml% "%temp%\WEB-INF\"
mkdir "%temp%\WEB-INF\classes\"
@REM Compiler les fichiers .java
for /R "%src%" %%f in (*.java) do (
    javac -cp "%lib%\*;%src%" -d "%temp%\WEB-INF\classes" "%%f"
)

@REM get my folder name
for /D %%i in ("%cd%") do set "myfolder=%%~nxi"

@REM Créer un fichier .war à partir du dossier temp
jar -cvf %myfolder%.war -C "%temp%" .

@REM Déplacer le fichier .war vers le dossier webapps de Tomcat
move %myfolder%.war "%webapps%"

endlocal