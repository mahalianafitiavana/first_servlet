<%@ page import="info.*,java.util.List" %>
<%
    String b =  (String) request.getAttribute("modelejson");
    List<Brand> list  =  (List) request.getAttribute("brandlist");
    List<Storage> liststorage  =  (List) request.getAttribute("storagelist");
%>

<html>
<head>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />
    <title>New Brand</title>
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="./assets/img/favicon/favicon.ico" />
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="./assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="./assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="./assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="./assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <%-- Script --%>
    <script type="text/javascript">
        var modelejson =  '<%=  b %>';
    </script>
    <script src="./assets/js/modeleupdate.js"  > </script>
</head>
<body>
    <div class="col-md-4" style="margin : auto; ">
        <div class="col-md-12 " style=" align: center">
            <div class="card mb-4" style="margin-top: 50px">
                <h5 class="card-header" >Add New Modele</h5>
                <form action= "modele" method="post">
                    <div class="card-body">
                        <div class="mb-3">
                            <input type="hidden"  name="id" value="-1" >
                            <label  class="form-label">Name </label>
                            <input
                                class="form-control"save
                                type="text"
                                id="exampleFormControlReadOnlyInput1"
                                placeholder="ex: Serie A54"
                                name="name"
                                 />
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Brand </label>
                            <select name = "idbrand" id="largeSelect" class="form-select form-select-lg">
                                <% for(Brand brand : list) { %>
                                    <option value="<%= brand.getId() %>"><%= brand.getName() %></option>
                                <% } %>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Storage </label>
                            <select name = "idstorage" id = "idstorage"  class="form-select form-select-lg">
                                <% for(Storage brand : liststorage) { %>
                                    <option value="<%= brand.getId() %>"><%= brand.getCapacity()+"Go" %></option>
                                <% } %>
                            </select>
                        </div>
                        <div class="d-grid gap-2 col-lg-6 mx-auto">
                            <button class="btn btn-primary btn-lg" type="submit"id="submit" >Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
<body>