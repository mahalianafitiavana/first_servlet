<%@ page import="java.util.List,info.*" %>
<%
    List<Phone> phone  =  (List) request.getAttribute("phonelist");
     List<Brand> brands  =  (List) request.getAttribute("brandlist");
    List<Status> status  =  (List) request.getAttribute("statuslist");
%>
<!DOCTYPE html>
<html lang="fr">
 <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <title>Phone</title>
    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="./assets/img/favicon/favicon.ico" />
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="./assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="./assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="./assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="./assets/css/nav.css" />
    <!-- Custom CSS for search form -->
    <style>
      .search-form {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 20px;
      }
      .search-form input, .search-form select {
        margin-right: 10px;
      }
    </style>
 </head>
 <body>  
        <div class="row">
            <nav class="col-md-2 d-none d-md-block vertical-menu">
                    <a href="phone">Phone</a>
                    <a href="modele">Modele</a>
                     <a href="sale">Sale</a>
                     <a href="repair">Repair</a>
            </nav>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
        
                <h1>Phones</h1>
                <!-- Search form -->
                <form class="search-form" action="phone" method="get">
                    <input type="hidden" name="surch" value="0" placeholder="Min Price"  />
                    <input type="number" name="min" placeholder="Min Price" />
                    <input type="number" name="max" placeholder="Max Price" />
                    <select name="status">
                        <option value="null">Status List</option>
                            <% for(Status s : status) { %>
                                <option value="<%= s.getId() %>"><%= s.getDesc() %></option>
                            <% } %>                    
                    </select>
                    <select name="brand">
                        <option value="null" >Brand List</option>
                        <% for(Brand brand : brands) { %>
                                    <option value="<%= brand.getId() %>"><%= brand.getName() %></option>
                                <% } %>
                    </select>
                    <button type="submit">Search</button>
                </form>
            <div class="card">
            <h5 class="card-header">Phone List </h5>
            <div class="table-responsive text-nowrap">
                <table class="table">
                <caption class="ms-4">
                    Phone List
                </caption>
                <thead>
                    <tr>
                        <th>Brand</th>
                        <th>Modele</th>
                        <th>Storage</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <% for(Phone p : phone) { %>
                    <tr>
                        <td>
                            <strong>
                            <%
                                Brand b = new Brand(); b.setId(p.getBrand()); b.getById();
                                out.print(b.getName());
                            %>
                            </strong>
                        </td>
                        <td>
                            <%
                                Modele m = new Modele(); m.setId(p.getModele()); m.getById();
                                out.print(m.getName());
                            %>
                        </td>
                         <td>
                            <%
                                Storage s = new Storage(); s.setId(m.getStorage()); s.getById();
                                out.print(s.getCapacity()+"Go");
                            %>
                        </td>
                        <td>
                            <%= p.getPrice()+"$"%>
                        </td>
                        <td>
                            <%
                                Status st = new Status(); st.setId(p.getStatus()); st.getById();
                                out.print(st.getDesc());
                            %>
                        </td>
                        <td>
                            <a href="phoneForm?action=update&id=<%= p.getId() %>"><i class="bx bx-edit-alt me-2"></i> </a>
                            <a href="phone?action=delete&id=<%= p.getId() %>"><i class="bx bx-trash me-2"></i> </a>
                            <a href="phoneForm"><i class="bx bxs-add-to-queue me-2"></i> </a>
                        </td>
                    </tr>
                    <% } %>
                    
                </tbody>
                </table>
            </main>
        </div>
 </body>
</html>
