<%@ page import="java.util.List,info.*" %>
<%
    List<Repair> repairs  =  (List) request.getAttribute("repairlist");
    List<Employe> employes  =  (List) request.getAttribute("listemploye");   
    List<Rep_status> status  =  (List) request.getAttribute("statuslist");
%>
<!DOCTYPE html>
<html lang="fr">
 <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <title>Repair</title>
    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="./assets/img/favicon/favicon.ico" />
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="./assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="./assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="./assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="./assets/css/nav.css" />
    <!-- Custom CSS for search form -->
    <style>
      .search-form {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 20px;
      }
      .search-form input, .search-form select {
        margin-right: 10px;
      }
    </style>
 </head>
 <body>  
        <div class="row">
            <nav class="col-md-2 d-none d-md-block vertical-menu">
                    <a href="phone">Phone</a>
                    <a href="modele">Modele</a>
                     <a href="sale">Sale</a>
                    <a href="repair">Repair</a>
            </nav>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
        
                <h1>Repairs</h1>
                <form class="search-form" action="repair" method="get">
                    <input type="hidden" name="surch" value="0" />
                    <input type="number" name="min" placeholder="Min Price" />
                    <input type="number" name="max" placeholder="Max Price" />
                    <select name="employe">
                        <option value="null">Employe List</option>
                            <% for(Employe e : employes) { %>
                                <option value="<%= e.getId() %>"><%= e.getNom() %></option>
                            <% } %>                    
                    </select>
                    <select name="rep_status">
                        <option value="null" >Status </option>
                        <% for(Rep_status brand : status) { %>
                            <option value="<%= brand.getId() %>"><%= brand.getDesc() %></option>
                        <% } %>
                    </select>
                    <button type="submit">Search</button>
                </form>
            <div class="card">
            <h5 class="card-header">Repair List </h5>
            <div class="table-responsive text-nowrap">
                <table class="table">
                <caption class="ms-4">
                    Repair List
                </caption>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Phone</th>
                        <th>Employe</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <% for(Repair r : repairs) { %>
                    <tr>
                        <td>
                            <strong>
                             <%= r.getRepairDate() %>
                            </strong>
                        </td>
                        <td>
                            <%
                                Phone p = new Phone(); p.setId(r.getIdphone()); p.getById();
                                Modele m =  new Modele(); m.setId(p.getModele()); m.getById();
                                Brand b = new Brand(); b.setId(m.getBrand()); b.getById();
                                Storage st =  new Storage(); st.setId(m.getStorage()); st.getById();
                                out.print(b.getName()+"   "+m.getName()+"  "+st.getCapacity()+"Go");
                            %>
                        </td>
                        </td>
                         <td>
                            <%
                                Employe e = new Employe(); e.setId(r.getIdemploye()); e.getById();
                                out.print(e.getNom());
                            %>
                        </td>
                        <td>
                            <%= r.getCost()+"$"%>
                        </td>
                        <td>
                            <%
                                Rep_status stt = new Rep_status(); stt.setId(r.getIdstatus()); stt.findById();
                                out.print(stt.getDesc());
                            %>
                        </td>
                        <td>
                           <%= r.getDescription() %>
                        </td>
                        <td>
                            <a href="repairForm?action=update&id=<%= r.getIdrepair() %>"><i class="bx bx-edit-alt me-2"></i> </a>
                            <a href="repair?action=delete&id=<%= r.getIdrepair() %>"><i class="bx bx-trash me-2"></i> </a>
                            <a href="repairForm"><i class="bx bxs-add-to-queue me-2"></i> </a>
                        </td>
                    </tr>
                    <% } %>
                    
                </tbody>
                </table>
            </main>
        </div>
 </body>
</html>
