<%@ page import="java.util.List,info.*" %>
<%
    List<Modele> list = (List) request.getAttribute("list");
    List<Brand> brand = (List) request.getAttribute("listbrand");
%>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />
    <title>Brand</title>
    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="./assets/img/favicon/favicon.ico" />
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="./assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="./assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="./assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="./assets/css/nav.css" />
    <style>
        .search-form {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }
        .search-form input, .search-form select {
            margin-right: 10px;
        }
    </style>
</head>
<body>        
    <div class="row">
        <nav class="col-md-2 d-none d-md-block vertical-menu">
                <a href="phone">Phone</a>
                <a href="modele">Modele</a>
                 <a href="sale">Sale</a>
                 <a href="repair">Repair</a>
        </nav>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h1 >Modeles </h1 >
                <form class="search-form" action="modele" method="get">
                    <input type="hidden" name="surch"  value="0"  />
                    <input type="text" name="name"  placeholder="Model Name"/>
                    <input type="number" name="min"  placeholder="Storage min"/>
                    <input type="number" name="max" placeholder="Storage max" />
                    <select name = "brand"  class="form-select">
                        <option value="null">Brand List</option>
                        <% for(Brand b : brand) { %>
                            <option value="<%= b.getId() %>"><%= b.getName() %></option>
                        <% } %>
                    </select>
                    <button type="submit">Search</button>
                </form>
        <div class="row">
        <div class="card">
            <h5 class="card-header">Modele List </h5>
            <div class="table-responsive text-nowrap">
                <table class="table">
                <caption class="ms-4">
                    Modele List
                </caption>
                <thead>
                    <tr>
                        <th>Brand</th>
                        <th>Modele</th>
                        <th>Storage</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <% for(Modele m : list) { %>
                    <tr>
                        <td>
                            <strong>
                                <%
                                    Brand b = new Brand(); b.setId(m.getBrand()); b.getById();
                                    out.print(b.getName());
                                %>
                            </strong>
                        </td>
                        <td><%= m.getName() %></td>
                        <td>
                            <%
                                Storage s = new Storage(); s.setId(m.getStorage()); s.getById();
                                out.print(s.getCapacity()+"Go");
                            %>
                        </td>
                        <td>
                            <a  id="update" href="modeleForm?action=update&id=<%= m.getId() %>"><i class="bx bx-edit-alt me-2"></i> </a>
                            <a href="modele?action=delete&id=<%= m.getId() %>" ><i class="bx bx-trash me-2"></i> </a>
                            <a href="modeleForm"><i class="bx bxs-add-to-queue me-2"></i> </a>
                        </td>
                    </tr>
                    <% } %>
                    
                </tbody>
                </table>
            </div>
        </div>
    </main>
</body>
</html>
