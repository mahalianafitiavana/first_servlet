window.addEventListener("load", function(){
    console.log(modelejson);
    modifyTitle();
    printValuesUpdating(modelejson);    
});
function modifyTitle(){
    let p = document.getElementsByTagName('h5')[0];
    p.innerHTML = "Modify Modele";
}
function printValuesUpdating(modelejson) {
    let modeleobject = JSON.parse(modelejson);
    let inputid  = document.getElementsByName('id')[0];
    inputid.value = modeleobject.id;
    let inputname = document.getElementsByName('name')[0];
    inputname.value = modeleobject.name;    
    let buttonsubmit = document.getElementById("submit");
    buttonsubmit.innerHTML = 'Modify';
    checkedBrand(modeleobject.brand);
    checkedStorage(modeleobject.storage);
}
function checkedBrand(id){
    let options = document.getElementById("largeSelect").options;
    for (let i = 0; i < options.length; i++) {
        if (options[i].value == id) {
            options[i].selected = true;
            break;
        }
    }
}
function checkedStorage(id) {
    let options = document.getElementById("idstorage").options; 
    for (let i = 0; i < options.length; i++) {
        console.log(options[i].value + "   " + id);
        if (options[i].value == id) {
            options[i].selected = true;
            break; 
        }
    }
}
