window.addEventListener("load", function () {
    let storagelist = JSON.parse(storagejson);
    let selectedBrand = document.getElementsByName("idbrand")[0];
    selectedBrand.addEventListener("change", function () {
        getModeleList(selectedBrand.value, storagelist);
    });
    // Récupérer toutes les listes déroulantes de la page
    if (phonejson === 'null') {
        // tsy manao inin
    } else {
        modifyTitle();
        printValuesUpdating(phonejson, storagelist);
    }
    var listesDeroulantes = document.getElementsByTagName('select');

    // Parcourir toutes les listes déroulantes
    for (var i = 0; i < listesDeroulantes.length; i++) {
        var listeDeroulante = listesDeroulantes[i];

        // Récupérer toutes les options de la liste déroulante
        var options = listeDeroulante.options;
        let p = 0;
        // Parcourir toutes les options
        console.log(options.length);
        for (let id = 0; id < options.length; id++) {
            const element = options[id]; 
            console.log(element.value+" "+id);
            
        }
    }
});

function modifyTitle() {
    let p = document.getElementsByTagName('h5')[0];
    p.innerHTML = "Modify Phone";
    // modify button add to modify
    let buttonsubmit = document.getElementById("submit");
    buttonsubmit.innerHTML = 'Modify';
}

function selectOptionByValue(select, value) {
    for (let i = 0; i < select.options.length; i++) {
        if (select.options[i].value == value) {
            select.options[i].selected = true;
            return;
        }
    }
}

function getModeleList(idbrand, storagelist) {
    let xhr = getxhr();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                let modelList = JSON.parse(xhr.responseText);
                printModelbyIdBrand(modelList, storagelist);
            } else {
                document.dyn = "Error code " + xhr.status;
            }
        }
    };

    xhr.open("POST", "phoneForm", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("idbrand=" + idbrand);
}

function printModelbyIdBrand(modeleList, storagelist) {
    let select = document.getElementsByName('idmodele')[0];
    clearSelectOptions(select);

    for (let id = 0; id < modeleList.length; id++) {
        const model = modeleList[id];
        const option = document.createElement('option');
        option.value = model.id;

        let storage = getStoragebyId(model.storage, storagelist);
        option.text = `${model.name} (${storage.capacity}Go)`;
        select.appendChild(option); 
    }

}

function clearSelectOptions(select) {
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
}

function getStoragebyId(id, storagelist) {
    for (let i = 0; i < storagelist.length; i++) {
        const storage = storagelist[i];
        if (id == storage.id) {
            return storage;
        }
    }
}

function printValuesUpdating(phonejson, storagelist) {
    let object = JSON.parse(phonejson);
    let inputid = document.getElementsByName('id')[0];
    inputid.value = object.id;

    let price = document.getElementsByName('price')[0];
    price.value = object.price;

    // Get Model idbrand and print
    getModeleList(object.brand, storagelist);
    selectOptionByValue(document.getElementsByName('idmodele')[0], object.modele);
    let modele = document.getElementsByName("idmodele")[0];
    modele.disabled = false;

    // Select status, brand, and modele
    selectedStatus(object.status);
    selectedBrand(object.brand);
}

function selectedStatus(idstatus) {
    let options = document.getElementsByName("idstatus")[0].options;
    selectOption(options, idstatus);
}

function selectedBrand(idbrand) {
    let options = document.getElementsByName("idbrand")[0].options;
    selectOption(options, idbrand);
}

function selectedModele(idmodele) {
    let options = document.getElementsByName("idmodele")[0].options;
    selectOption(options, idmodele);
}

function selectOption(options, valueToSelect) {
    for (let i = 0; i < options.length; i++) {
        options[i].selected = options[i].value == valueToSelect;
    }
}
