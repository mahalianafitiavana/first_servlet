<%@ page import="java.io.IOException" %>
<%@ page import="info.*,java.util.List" %>
<%
    List<Brand> brands  =  (List) request.getAttribute("brandlist");
    List<Status> status  =  (List) request.getAttribute("statuslist");
%>

<html>
<head>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />
    <title>New Phone</title>
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="./assets/img/favicon/favicon.ico" />
    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="./assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="./assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="./assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />
    <link rel="stylesheet" href="./assets/css/demo.css" />
    <!-- Vendors CSS -->
    <link rel="stylesheet" href="./assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <%-- Script --%>
    <script type="text/javascript">
        var phonejson =  '<%= (String)  request.getAttribute("phonejson") %>';
        var storagejson =  '<%= (String)  request.getAttribute("storagelist") %>';
    </script>
    <script src="./assets/js/xhr.js"  > </script>
    <script src="./assets/js/phoneupdate.js"  > </script>
</head>
<body>
    <div class="col-md-4" style="margin : auto; ">
        <div class="col-md-12 " style="align:center">
            <div class="card mb-4" style="margin-top: 50px">
                <h5 class="card-header">Add New Phone</h5>
                <form action= "phone" method="post">
                    <div class="card-body">
                        <div class="mb-3">
                            <input type="hidden"  name="id" value="-1" >
                            <label  class="form-label">Price </label>
                            <input
                                class="form-control"
                                type="number"
                                placeholder=" ex: 1000$"
                                name="price"
                                 />
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Status </label>
                            <select name="idstatus"  class="form-select form-select-lg largeSelect">
                                <% for(Status s : status) { %>
                                    <option value="<%= s.getId() %>"><%= s.getDesc() %></option>
                                <% } %>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Brand </label>
                            <select name="idbrand" class="largeSelect form-select form-select-lg">
                                    <option>Brand List</option>
                                <% for(Brand brand : brands) { %>
                                    <option value="<%= brand.getId() %>"><%= brand.getName() %></option>
                                <% } %>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label  class="form-label">Modele </label>
                            <select name="idmodele" class="form-select form-select-lg" >
                                    <option>Modele List</option>
                            </select>
                        </div>
                        <div class="d-grid gap-2 col-lg-6 mx-auto">
                            <button class="btn btn-primary btn-lg" type="submit"id="submit" >Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
<body>